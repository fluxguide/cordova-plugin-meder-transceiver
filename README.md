# Cordova Plugin Meder Transceiver

## Installation

- Clone the Repository to your computer: `git clone git@bitbucket.org:fluxguide/cordova-plugin-meder-transceiver.git`
- Add the plugin: `cordova plugin add /path/to/cloned/directory`
- After installation you are required to add the kotlin-stdlib as an dependency:
    In Android Studio → File → Project Structure → Modules: app → Dependencies → Add (+) → Library Dependency → search for "kotlin-stdlib" and add the most recent, like "org.jetbrains.kotlin:kotlin-stdlib:1.3.41"

## Requirements

- Android SDK Level >= 27 (Android 8.1)
- Permission: `com.medercommtech.sgrta.RTXSERVICE` (automatically added by this plugin)

## Methods Summary

- [MederTransceiver.setup()](#Setup)
- [MederTransceiver.change_channel()](#Change-Channel)
- [MederTransceiver.set_volume()](#Set-Volume)
- [MederTransceiver.set_mode()](#Set-Mode)
- [MederTransceiver.send_data()](#Send-Data)
- [MederTransceiver.set_rtx_path()](#Set-RTX-Path)
- [MederTransceiver.set_mic_noise_gate()](#Set-Mic-Noise-Gate)
- [MederTransceiver.set_mic_sensitivity()](#Set-Mic-Sensitivity)
- [MederTransceiver.unbind_rtx_service()](#Unbind-RTX-Service)
- [MederTransceiver.enable_ir_receive_mode()](#enable-ir-receive-mode)
- [MederTransceiver.disable_ir_receive_mode()](#disable-ir-receive-mode)
- [MederTransceiver.send_ir_command()](#send-ir-command)


---


## Events Summary 

- [`medertransceiver_datareceived`](#medertransceiver_datareceived)
- [`medertransceiver_rssireceived`](#medertransceiver_rssireceived)
- [Additional Events](#Additional-Events)



---

## Modes

- `MederTransceiver.mode.NONE` - The RTX module is not active. It can not receive or send speech or commands.
- `MederTransceiver.mode.COMMAND_RECEIVE` - The RTX module is in command receive mode.
- `MederTransceiver.mode.COMMAND_SEND` - The RTX module is in command send mode.
- `MederTransceiver.mode.LISTEN_HEADPHONES` - The RTX module is in RX/listen mode. It uses external headphones for the audio output.
- `MederTransceiver.mode.LISTEN_SPEAKER_BACK` - The RTX module is in RX/listen mode. It uses the back speaker for the audio output.
- `MederTransceiver.mode.LISTEN_SPEAKER_FRONT` - The RTX module is in RX/listen mode. It uses the front speaker for the audio output.
- `MederTransceiver.mode.SPEAK_EXTERNAL_MIC` - The RTX module is in TX/speak mode. It uses an external mic for the speech. 
- `MederTransceiver.mode.SPEAK_INTERNAL_MIC` - The RTX module is in TX/speak mode. It uses the internal mic for the speech.

---

### Setup

Before interacting with any of the other methods, you need to call .setup()

```js
MederTransceiver.setup(success_callback, error_callback);
```

#### Parameters
- {function} success_callback - The callback which will be called when setup was successful.
- {function} error_callback - The callback which will be called when setup  encounters an error. The function is passed a single string parameter containing the error message.

#### Example Usage
```js
MederTransceiver.setup(function(success) {
    console.log("MederTransceiver setup success");
}, function (error) {
    console.log("MederTransceiver setup error", error);
});
```

---


### Change Channel

```js
MederTransceiver.change_channel(channel_number, success_callback, error_callback);
```

#### Parameters
- {number} `channel_number` - The channel to set the device to. Must be within range **1-30**
- {function} `success_callback` - The callback which will be called when change_channel was successful.
- {function} `error_callback` - The callback which will be called when change_channel  encounters an error. The function is passed a single string parameter containing the error message.

#### Example Usage
```js
MederTransceiver.change_channel(
    12, 
    function(success) {
        console.log("change_channel success");
    },
    function (error) {
        console.log("change_channel error", error);
    });

```


---


### Set Volume

Set the volume for audio stream (in "Receive Voice" mode)

```js
MederTransceiver.set_volume(volume, success_callback, error_callback);
```


#### Parameters

- {number} `volume` - The new volume. Must be between **0-1** (0 = mute, 1 = max volume)
- {function} `success_callback` - The callback which will be called when set_volume was successful.
- {function} `error_callback` - The callback which will be called when set_volume  encounters an error. The function is passed a single string parameter containing the error message.


#### Example Usage

```js
MederTransceiver.set_volume(0.5, function(success) {
    console.log("set_volume success");
}, function (error) {
    console.log("set_volume error", error);
});

```

---


### Set Mode

Use this function to set the Transceiver in the correct mode.


```js
MederTransceiver.set_mode(mode, success_callback, error_callback);
```


#### Parameters

- {string} `mode` - The RTX Mode. One of:
  - MederTransceiver.rtxmode.NONE
  - MederTransceiver.rtxmode.COMMAND_RECEIVE
  - MederTransceiver.rtxmode.COMMAND_SEND
  - MederTransceiver.rtxmode.LISTEN_HEADPHONES
  - MederTransceiver.rtxmode.LISTEN_SPEAKER_BACK
  - MederTransceiver.rtxmode.LISTEN_SPEAKER_FRONT
  - MederTransceiver.rtxmode.SPEAK_EXTERNAL_MIC
  - MederTransceiver.rtxmode.SPEAK_INTERNAL_MIC
- {function} `success_callback` - The callback which will be called when send_data was successful.
- {function} `error_callback` - The callback which will be called when send_data  encounters an error. The function is passed a single string parameter containing the error message.


#### Example Usage

```js
MederTransceiver.set_mode(MederTransceiver.rtxmode.LISTEN_HEADPHONES, function(success) {
    console.log("set_mode success");
}, function (error) {
    console.log("set_mode error", error);
});

```

---


### Send Data

Use this function to send a data package.

**Important:** MederTransceiver must be in "Send Data" oder "Send Voice" mode.


```js
MederTransceiver.send_data(data, success_callback, error_callback);
```


#### Parameters

- {string} `data` - The data payload. Can be **max 16 byte** when the device is in "Send Voice" mode, or **max 80 byte** when the device is in "Send Data" Mode.
- {function} `success_callback` - The callback which will be called when send_data was successful.
- {function} `error_callback` - The callback which will be called when send_data  encounters an error. The function is passed a single string parameter containing the error message.


#### Example Usage

```js
MederTransceiver.send_data("Hello World", function(success) {
    console.log("send_data success");
}, function (error) {
    console.log("send_data error", error);
});

```

---


### Set RTX Path

There are two Audio Paths for Listening: ANALOG and DIGITAL:
- ANALOG: very fast (no latency) but low audio quality
- DIGITAL: better quality, but significant delay

```js
MederTransceiver.set_rtx_path(path, success_callback, error_callback);
```


#### Parameters

- {string} `path` - The RTX Audio Path. One of:
  - MederTransceiver.rtxpaths.ANALOG
  - MederTransceiver.rtxpaths.DIGITAL
- {function} `success_callback` - The callback which will be called when set_rtx_path was successful.
- {function} `error_callback` - The callback which will be called when set_rtx_path  encounters an error. The function is passed a single string parameter containing the error message.


#### Example Usage

```js
MederTransceiver.set_rtx_path(MederTransceiver.rtxpaths.DIGITAL, function(success) {
    console.log("set_rtx_path success");
}, function (error) {
    console.log("set_rtx_path error", error);
});
```


---



### Set Mic Noise Gate

Enable/Disable the Noise Gate for the Microphone (when in mode SPEAK_EXTERNAL_MIC or SPEAK_INTERNAL_MIC)

```js
MederTransceiver.set_mic_noise_gate(enabled, success_callback, error_callback);
```


#### Parameters

- {boolean} `enabled` - If set to `true`, the Noise Gate will be active.
- {function} `success_callback` - The callback which will be called when set_mic_noise_gate was successful.
- {function} `error_callback` - The callback which will be called when set_mic_noise_gate encounters an error. The function is passed a single string parameter containing the error message.


#### Example Usage

```js
MederTransceiver.set_mic_noise_gate(true, function(success) {
    console.log("set_mic_noise_gate success");
}, function (error) {
    console.log("set_mic_noise_gate error", error);
});

```

---



### Set Mic Sensitivity

Set the sensitivity for the microphone (when in mode SPEAK_EXTERNAL_MIC or SPEAK_INTERNAL_MIC)

```js
MederTransceiver.set_mic_sensitivity(sensitivity, success_callback, error_callback);
```


#### Parameters

- {number} `sensitivity` - Can be in the range 1 - 7. 1 for low sensitivity and 7 for high sensitivity. The default value is 3.
- {function} `success_callback` - The callback which will be called when set_mic_sensitivity was successful.
- {function} `error_callback` - The callback which will be called when set_mic_sensitivity encounters an error. The function is passed a single string parameter containing the error message.


#### Example Usage

```js
MederTransceiver.set_mic_sensitivity(true, function(success) {
    console.log("set_mic_sensitivity success");
}, function (error) {
    console.log("set_mic_sensitivity error", error);
});

```
---



### Unbind RTX Service

Unbind the RTX Service. Could be useful for re-initializing the RTX Service.

```js
MederTransceiver.unbind_rtx_service(success_callback, error_callback);
```


#### Parameters

- {function} `success_callback` - The callback which will be called when unbind_rtx_service was successful.
- {function} `error_callback` - The callback which will be called when unbind_rtx_service encounters an error. The function is passed a single string parameter containing the error message.


#### Example Usage

```js
MederTransceiver.unbind_rtx_service(true, function(success) {
    console.log("unbind_rtx_service success");
}, function (error) {
    console.log("unbind_rtx_service error", error);
});

```

---

### Enable IR Receive Mode

Enable IR Receive Mode. Must be called before any IR messages can be received.

```js
MederTransceiver.enable_ir_receive_mode(success_callback, error_callback);
```


---

### Disable IR Receive Mode

Disable IR Receive Mode.

```js
MederTransceiver.disable_ir_receive_mode(success_callback, error_callback);
```

---

### Send IR Command

Send an IR command. Requirements for the command:
- Must be two characters long
- Characters must be in the range of a-z and 0-9
- Must be two different characters (!!)

```js
MederTransceiver.send_ir_command(command, success_callback, error_callback);
```






---

## Events

### `medertransceiver_datareceived`

```
document.addEventListener('medertransceiver_datareceived', (event) => {
    console.log("data: ", event.data);
});
```

---

### `medertransceiver_rssireceived`

Will be fired when a RSSI Package is received. 

`rssi` is a value between approx. **-120 and -20**, where -120 is very far away and -20 is very near.

```
document.addEventListener('medertransceiver_rssireceived', (event) => {
    console.log("result: ", event.rssi);
});
```

---

### `medertransceiver_irdatareceived`

Will be fired when an Infrared Message is received.

```
document.addEventListener('medertransceiver_irdatareceived', (event) => {
    console.log("data: ", event.data);
});
```

---

#### Additional Events: 
- `medertransceiver_serviceconnected`
- `medertransceiver_servicedisconnected`
- `medertransceiver_serviceerror`
- `medertransceiver_replyerrorreceived`
- `frequencyrangereceived`

IR additional events:
- `medertransceiver_irserviceconnected`
- `medertransceiver_irserviceready`
- `medertransceiver_irservicedisconnected`
- `medertransceiver_serviceerror`