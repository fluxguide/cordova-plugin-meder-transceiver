package com.fluxguide.cordova.plugin;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import com.medercommtech.smartguide.irservice.IRInvalidCmdException;
import com.medercommtech.smartguide.irservice.IRMessageReceivedListener;
import com.medercommtech.smartguide.irservice.IRServiceConnectionException;
import com.medercommtech.smartguide.irservice.IRServiceEventListener;
import com.medercommtech.smartguide.rtxserviceapi.RTXConfigurationException;
import com.medercommtech.smartguide.rtxserviceapi.RTXMode;
import com.medercommtech.smartguide.rtxserviceapi.RTXPath;
import com.medercommtech.smartguide.rtxserviceapi.RTXServiceAPI;
import com.medercommtech.smartguide.rtxserviceapi.RTXServiceConnectionException;

import com.medercommtech.smartguide.irservice.IRServiceAPI;


import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaWebView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class MederTransceiver extends CordovaPlugin {

    // the current callback-context
    private CallbackContext _callbackContext;

    // TAG used for logging to Logcat
    private static final String TAG = "MederTransceiver";

    // Define all available actions
    private static final String ACTION_SETUP = "SETUP";
    private static final String ACTION_CHANGE_CHANNEL = "CHANGE_CHANNEL";
    private static final String ACTION_SEND_COMMAND = "SEND_COMMAND";
    private static final String ACTION_SET_VOLUME = "SET_VOLUME";
    private static final String ACTION_SET_RTX_MODE = "SET_RTX_MODE";
    private static final String ACTION_SET_RTX_PATH = "SET_RTX_PATH";

    private static final String ACTION_SET_MIC_NOISE_GATE = "SET_MIC_NOISE_GATE";
    private static final String ACTION_SET_MIC_SENSITIVITY = "SET_MIC_SENSITIVITY";
    private static final String ACTION_UNBIND_RTX_SERVICE = "UNBIND_RTX_SERVICE";


    private static final String ACTION_ENABLE_IR_RECEIVE_MODE = "ENABLE_IR_RECEIVE_MODE";
    private static final String ACTION_DISABLE_IR_RECEIVE_MODE = "DISABLE_IR_RECEIVE_MODE";
    private static final String ACTION_SEND_IR_COMMAND = "SEND_IR_COMMAND";




    private RTXServiceAPI rtxServiceApi;
    private RTXServiceEventListenerListener myRTXServiceEventListener;
    private RTXMessageReceivedListenerListener myRTXMessageReceivedListener;
    private Map<String, RTXMode> rtxModeMap;

    private IRServiceAPI irServiceAPI;
    private IRServiceEventListener myIRServiceEventListener;
    private IRMessageReceivedListener myIRMessageReceivedListener;


    /**
     * Initialize the Plugin
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {

        // your init code here

        super.initialize(cordova, webView);

        this._callbackContext = null;
        this.rtxServiceApi = null;
        this.irServiceAPI = null;

        Log.i(TAG, "INITIALIZES");

        this.rtxModeMap = new HashMap<String, RTXMode>();
        this.rtxModeMap.put("COMMAND_RECEIVE", RTXMode.COMMAND_RECEIVE);
        this.rtxModeMap.put("COMMAND_SEND", RTXMode.COMMAND_SEND);
        this.rtxModeMap.put("LISTEN_HEADPHONES", RTXMode.LISTEN_HEADPHONES);
        this.rtxModeMap.put("LISTEN_SPEAKER_BACK", RTXMode.LISTEN_SPEAKER_BACK);
        this.rtxModeMap.put("LISTEN_SPEAKER_FRONT", RTXMode.LISTEN_SPEAKER_FRONT);
        this.rtxModeMap.put("NONE", RTXMode.NONE);
        this.rtxModeMap.put("SPEAK_EXTERNAL_MIC", RTXMode.SPEAK_EXTERNAL_MIC);
        this.rtxModeMap.put("SPEAK_INTERNAL_MIC", RTXMode.SPEAK_INTERNAL_MIC);

    }

    //@Override
    public void onDestroy() {
        // destroy code here
        if (this.rtxServiceApi != null) {
            this.rtxServiceApi.unbindRTXService(false);
        }
        if (this.irServiceAPI != null) {
            this.irServiceAPI.unbindIRService(false);
        }
        super.onDestroy();
    }


    /**
     * Execute: this is the main function that gets called from JS
     *
     * @param action          The action to execute
     * @param args            JSONArray of arguments
     * @param callbackContext the Callback Context back to JS
     * @return true if action was valid, false if not
     * @throws JSONException
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Log.i(TAG, "EXECUTE " + action);
        // set callbackcontext
        this._callbackContext = callbackContext;


        // ACTION_SETUP
        if (action.equals(ACTION_SETUP)) {

            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    Looper.prepare();
                    setup();
                    Looper.loop();
                }
            });
            //this.setup(callbackContext);

            return true;
        }

        // ACTION_CHANGE_CHANNEL
        if (action.equals(ACTION_CHANGE_CHANNEL)) {
            int channel = args.getInt(0);
            this.changeChannel(channel);
            return true;
        }

        // ACTION_SET_VOLUME
        if (action.equals(ACTION_SET_VOLUME)) {
            int volume = args.getInt(0);
            this.setVolume(volume);
            return true;
        }

        // ACTION_SET_MODE
        if (action.equals(ACTION_SET_RTX_MODE)) {
            String input_mode = args.getString(0);
            this.setRTXMode(input_mode);
            return true;
        }

        // ACTION_SEND_COMMAND
        if (action.equals(ACTION_SEND_COMMAND)) {
            String message = args.getString(0);
            Log.i(TAG, "SEND COMMAND " + message);
            this.sendCommand(message);
            return true;
        }

        // ACTION_SEND_COMMAND
        if (action.equals(ACTION_SET_RTX_PATH)) {
            String path = args.getString(0);
            this.setRTXPath(path);
            return true;
        }

        // ACTION_SET_MIC_NOISE_GATE
        if (action.equals(ACTION_SET_MIC_NOISE_GATE)) {
            Boolean enabled = args.getBoolean(0);
            this.setMicNoiseGate(enabled);
            return true;
        }

        // ACTION_SET_MIC_SENSITIVITY
        if (action.equals(ACTION_SET_MIC_SENSITIVITY)) {
            int sensitivity = args.getInt(0);
            this.setMicSensitivity(sensitivity);
            return true;
        }

        // ACTION_UNBIND_RTX_SERVICE
        if (action.equals(ACTION_UNBIND_RTX_SERVICE)) {
            this.unbindRTXService(false);
            return true;
        }

///// IR ACTIONS
        // ACTION_ENABLE_IR_RECEIVE_MODE
        if (action.equals(ACTION_ENABLE_IR_RECEIVE_MODE)) {
            this.enable_IR_receive_mode(true);
            Log.i(TAG, "ENABLE IR");
            return true;
        }
        // ACTION_DISABLE_IR_RECEIVE_MODE
        if (action.equals(ACTION_DISABLE_IR_RECEIVE_MODE)) {
            this.enable_IR_receive_mode(false);
            Log.i(TAG, "DISABLE IR");
            return true;
        }
        // ACTION_DISABLE_IR_RECEIVE_MODE
        if (action.equals(ACTION_SEND_IR_COMMAND)) {
            String command = args.getString(0);
            this.sendIRCommand(command);
            Log.i(TAG, "SEND IR COmmand");
            return true;
        }






        return true;
    }

    /**
     * Send an OK (Success) Message back to JS
     */
    private void send_OK() {
        if(this._callbackContext == null) return;
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);
    }

    /**
     * Send an Error back to JS
     * use this instead of this._callbackContext.error("ConfigurationException");
     */
    private void send_ERROR(String error_message) {
        if(this._callbackContext == null) return;
        PluginResult result = new PluginResult(PluginResult.Status.ERROR, error_message);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);
    }

    /**
     * Send a javascript command
     * Use it like this: this.sendJavascript("fg.app.temp.last_rtx_mode = 'yes';");
     * UNUSED AT THE MOMENT
     */
    protected void sendJavascript(final String javascript) {
        webView.getView().post(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:(function() { " + javascript + "})();");
            }
        });
    }

    /**
     * Setup the serviceAPI
     */
    private void setup() {
        Log.i(TAG, "Setup Start!");

        // get application context
        Context context = this.cordova.getActivity().getApplicationContext();

        // create RTXServiceEventListenerListener and RTXMessageReceivedListenerListener
        myRTXServiceEventListener = new RTXServiceEventListenerListener(this._callbackContext, this);
        myRTXMessageReceivedListener = new RTXMessageReceivedListenerListener(this._callbackContext, this);

        // create the serviceEventListener
        //if(this.rtxServiceApi != null) this.rtxServiceApi.unbindRTXService(true);
        this.rtxServiceApi = new RTXServiceAPI(context, myRTXServiceEventListener, myRTXMessageReceivedListener);

        // bind to RTX Service
        try {
            this.rtxServiceApi.bindRTXService();
        } catch (Error error) {
            this.send_ERROR("FAILED");
        }


        // create IR service api
        myIRServiceEventListener = new IRServiceEventListenerListener(this);
        myIRMessageReceivedListener = new IRMessageReceivedListenerListener(this);

        this.irServiceAPI = new IRServiceAPI(context, myIRServiceEventListener, myIRMessageReceivedListener);
        // bind to RTX Service
        try {
            this.irServiceAPI.bindIRService();
        } catch (Error error) {
            this.send_ERROR("FAILED");
        }

        // create a PluginResult to be able to keep the callbackContext alive
        this.send_OK();

        // this._callbackContext.success(); // Don't fire a callback-success, otherwise
        // the events wont work any more
    }

    /**
     * Set RTX Mode
     *
     * @param mode the mode, one of:
     *             RTXMode.COMMAND_RECEIVE - The RTX module is in command receive mode.
     *             RTXMode.COMMAND_SEND - The RTX module is in command send mode.
     *             RTXMode.LISTEN_HEADPHONES - The RTX module is in RX/listen mode. It uses external headphones for the audio output.
     *             RTXMode.LISTEN_SPEAKER_BACK - The RTX module is in RX/listen mode. It uses the back speaker for the audio output.
     *             RTXMode.LISTEN_SPEAKER_FRONT - The RTX module is in RX/listen mode. It uses the front speaker for the audio output.
     *             RTXMode.NONE - The RTX module is not active. It can not receive or send speech or commands.
     *             RTXMode.SPEAK_EXTERNAL_MIC - The RTX module is in TX/speak mode. It uses an external mic for the speech.
     *             RTXMode.SPEAK_INTERNAL_MIC - The RTX module is in TX/speak mode. It uses the internal mic for the speech.
     */
    private void setRTXMode(String mode) {

        // check if rtxServiceApi is set yet
        if (this.rtxServiceApi == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        // get the new RTXMode from our HashMap
        RTXMode newRTXMode = this.rtxModeMap.get(mode); // returns an RTXMode based on the key "mode"

        if (newRTXMode != null) {
            // setRTXMode
            try {
                this.rtxServiceApi.setRTXMode(newRTXMode);
                this.send_OK();

            } catch (RTXServiceConnectionException rtxServiceConnectionException) {
                this.send_ERROR("ServiceConnectionException");
            }
        } else {
            this.send_ERROR(mode + " is not a defined RTXMode");
        }

    }

    /**
     * Change to a channel
     *
     * @param channel
     */
    private void changeChannel(int channel) {

        // check if rtxServiceApi is set yet
        if (this.rtxServiceApi == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        // change channel
        try {
            this.rtxServiceApi.changeChannel(channel);
            this.send_OK();
        } catch (RTXConfigurationException rtxConfigurationException) {
            this.send_ERROR("ConfigurationException");
        } catch (RTXServiceConnectionException rtxServiceConnectionException) {
            this.send_ERROR("ServiceConnectionException");
        }
    }

    /**
     * Send a command
     *
     * @param message
     */
    private void sendCommand(String message) {

        // check if rtxServiceApi is set yet
        if (this.rtxServiceApi == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        try {
            byte[] messageBytes = message.getBytes();
            this.rtxServiceApi.sendCommand(messageBytes);
            this.send_OK();
        } catch (RTXConfigurationException rtxConfigurationException) {
            this.send_ERROR("ConfigurationException");
        } catch (RTXServiceConnectionException rtxServiceConnectionException) {
            this.send_ERROR("ServiceConnectionException");
        }
    }

    /**
     * Set the volume for Voice stream
     *
     * @param volume integer from 0-10 (0 = mute, 10 = max volume)
     */
    private void setVolume(int volume) {
        if (this.rtxServiceApi == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        try {
            this.rtxServiceApi.setRXVolume(volume);
            this.send_OK();
        } catch (RTXConfigurationException error) {
            this.send_ERROR("ConfigurationException");
        } catch (RTXServiceConnectionException error) {
            this.send_ERROR("ServiceConnectionException");
        }
    }

    private void setRTXPath(String path) {

        // check if rtxServiceApi is set yet
        if (this.rtxServiceApi == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        // Define the Path
        RTXPath myRTXPath;
        if (path.equals("ANALOG")) {
            myRTXPath = RTXPath.ANALOG;
        } else if (path.equals("DIGITAL")) {
            myRTXPath = RTXPath.DIGITAL;
        } else {
            // neither ANALOG nor DIGITAL -> erroor
            this.send_ERROR("Wrong Path, must be either ANALOG or DIGITAL");
            return;
        }

        this.rtxServiceApi.setRTXPath(myRTXPath);
        this.send_OK();

    }

    /**
     * Set the microphone noise gate on or off. Default value is off.
     * @param enabled
     */
    private void setMicNoiseGate(Boolean enabled) {

        // check if rtxServiceApi is set yet
        if (this.rtxServiceApi == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        try {
            this.rtxServiceApi.setTXMicNoiseGate(enabled);
            this.send_OK();
        } catch (RTXServiceConnectionException error) {
            this.send_ERROR("ServiceConnectionException");
        }
    }

    /**
     * Set the microphone sensitivity when in RTXMode.SPEAK_INTERNAL_MIC or RTXMode.SPEAK_EXTERNAL_MIC mode.
     * @param sensitivity Can be in the range 1 - 7. 1 for low sensitivity and 7 for high sensitivity. The default value is 3.
     */
    private void setMicSensitivity(int sensitivity) {

        // check if rtxServiceApi is set yet
        if (this.rtxServiceApi == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        try {
            this.rtxServiceApi.setTXMicSensitivity(sensitivity);
            this.send_OK();
        }
        catch (RTXConfigurationException error) {
            this.send_ERROR("ConfigurationException");
        }
        catch (RTXServiceConnectionException error) {
            this.send_ERROR("ServiceConnectionException");
        }
    }


    /**
     * This method unbinds from the RTX Service provided by the SGManager App. If no other activity is bound to the Service it gets destroyed and RTX is disabled.
     */
    private void unbindRTXService(boolean stopService) {
        // check if rtxServiceApi is set yet
        if (this.rtxServiceApi == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        // unbind
        this.rtxServiceApi.unbindRTXService(true);
        this.send_OK();

        Log.i(TAG, "Unbound");

    }




    /**
     * IR: enable IR receive mode
     */
    private void enable_IR_receive_mode(boolean enabled) {
        Log.i(TAG, "enable_IR_receive_mode");
        // check if irServiceAPI is set initialized
        if (this.irServiceAPI == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        try {
            this.irServiceAPI.enableReceiveMode(enabled);
        }
        catch(IRServiceConnectionException error) {
            this.send_ERROR("ServiceConnectionException");
        }
    }

    /**
     * IR: send ir command
     */
    private void sendIRCommand(String command) {
        Log.i(TAG, "send ir command: " + command);

        // check if irServiceAPI is set initialized
        if (this.irServiceAPI == null) {
            this.send_ERROR("ERROR_NOT_INITIALIZED");
            return;
        }

        try {
            this.irServiceAPI.sendCommand(command);
        }
        catch(IRServiceConnectionException error) {
            this.send_ERROR("ServiceConnectionException");
        }
        catch (IRInvalidCmdException error) {
            this.send_ERROR("IRInvalidCmdException");
        }

    }



}