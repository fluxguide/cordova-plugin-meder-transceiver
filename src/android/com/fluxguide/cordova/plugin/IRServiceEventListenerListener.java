package com.fluxguide.cordova.plugin;

import android.util.Log;

import com.medercommtech.smartguide.irservice.IRServiceEventListener;


public class IRServiceEventListenerListener implements IRServiceEventListener {

    private MederTransceiver _API;
    private static final String TAG = "MederTransceiver";

    public IRServiceEventListenerListener(MederTransceiver API) {
        this._API = API;
    }

    @Override
    public void onServiceConnected() {
        Log.i(TAG, "IRServiceEventListener: onServiceConnected");
        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_irserviceconnected');");
    }

    @Override
    public void onServiceReady() {
        Log.i(TAG, "IRServiceEventListener: onServiceReady");
        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_irserviceready');");
    }

    @Override
    public void onServiceDisconnected() {
        Log.i(TAG, "IRServiceEventListener: onServiceDisconnected");
        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_irservicedisconnected');");
    }

    @Override
    public void onServiceError(String error) {
        Log.i(TAG, "IRServiceEventListener: onServiceError");
        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_serviceerror');");
    }

}
