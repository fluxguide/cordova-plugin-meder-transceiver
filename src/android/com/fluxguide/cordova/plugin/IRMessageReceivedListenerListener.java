package com.fluxguide.cordova.plugin;

import com.medercommtech.smartguide.irservice.IRMessageReceivedListener;
import android.util.Log;


/**
 * a1 = 01
 * a2
 * a3
 * a4
 * a5
 * a6
 * a7
 * a8
 * a9
 * b0 = 10
 * b1
 * b2
 * b3
 * b4
 * b5
 * b6
 * b7
 * b8
 * b9
 * c0 = 20
 * c1
 * c2
 * c3
 * c4
 * c5
 * c6
 * c7
 * c8
 * c9
 * d0 = 30
 */

public class IRMessageReceivedListenerListener implements IRMessageReceivedListener {

    private MederTransceiver _API;
    private static final String TAG = "MederTransceiver";

    public IRMessageReceivedListenerListener(MederTransceiver API) {
        this._API = API;
        Log.i(TAG, "IRMessageReceivedListenerListener constructed");
    }

    @Override
    public void onCommandReceived(String command) {
        Log.i(TAG, "IRMessageReceivedListener commandReceived " + command);
        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_irdatareceived', {'data': '" + command + "'});");
    }

    @Override
    public void onIRReceiveModeEnabledReceived(boolean enabled) {
        // TODO
        Log.i(TAG, "IRMessageReceivedListener onIRReceiveModeEnabledReceived ");
    }

}
