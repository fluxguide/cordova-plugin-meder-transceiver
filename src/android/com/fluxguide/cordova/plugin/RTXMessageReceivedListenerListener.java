package com.fluxguide.cordova.plugin;

import com.medercommtech.smartguide.rtxserviceapi.RTXConfiguration;
import com.medercommtech.smartguide.rtxserviceapi.RTXFrequencyRange;
import com.medercommtech.smartguide.rtxserviceapi.RTXMessageReceivedListener;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

public class RTXMessageReceivedListenerListener implements RTXMessageReceivedListener {

    private CallbackContext _callbackContext;
    private MederTransceiver _API;

    public RTXMessageReceivedListenerListener(CallbackContext callbackContext, MederTransceiver API) {
        this._callbackContext = callbackContext;
        this._API = API;
        LOG.i("RTXMessageReceivedListenerListener", "callbackContext set in constructor!");
    }

    @Override
    public void onCommandReceived(byte[] bytes) {

        String s = new String(bytes);   // convert the message to string
        LOG.i("RTXMessageReceivedListenerListener", "commandReceived: " + s);
        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_datareceived', {'data': '" + s + "'});");

        // early exit if callbackContext is not yet set
        if(this._callbackContext == null) return;
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);

        // // the message object
        // JSONObject info = new JSONObject();
        // try {
        //     info.put("type", "data");
        //     info.put("data", s);

        //     // send PluginResult
        //     PluginResult result = new PluginResult(PluginResult.Status.OK, info);
        //     result.setKeepCallback(true);
        //     this._callbackContext.sendPluginResult(result);
        //     LOG.i("RTXMessageReceivedListenerListener", "commandReceived Result sent!");

        // } catch (JSONException e) {
        //     LOG.i("RTXMessageReceivedListenerListener", e.getMessage(), e);
        // }

    }

    @Override
    public void onFrequencyRangeReceived(RTXFrequencyRange rtxFrequencyRange) {

        String frequencyRangeString = "";
        if(rtxFrequencyRange == RTXFrequencyRange.F823) frequencyRangeString = "RTXFrequencyRange.F823";
        if(rtxFrequencyRange == RTXFrequencyRange.F863) frequencyRangeString = "RTXFrequencyRange.F863";

        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_frequencyrangereceived', {'frequencyrange': " + frequencyRangeString + "});");
        
        // early exit if callbackContext is not yet set
        if(this._callbackContext == null) return;
        // send PluginResult
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);

        // the message object
        // JSONObject info = new JSONObject();
        // try {
        //     info.put("type", "frequencyrangereceived");
        //     info.put("frequencyrange", frequencyRangeString);
        // } catch (JSONException e) {
        //     LOG.i("RTXMessageReceivedListenerListener", e.getMessage(), e);
        // }

    }

    @Override
    public void onReplyErrorReceived(String s, byte[] b) {

        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_replyerrorreceived', {'error': " + s + "});");

        // early exit if callbackContext is not yet set
        if(this._callbackContext == null) return;
        // send PluginResult
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);

        // // the message object
        // JSONObject info = new JSONObject();
        // try {
        //     info.put("type", "replyerrorreceived");
        //     info.put("error", s);
        // } catch (JSONException e) {
        //     LOG.i("RTXMessageReceivedListenerListener", e.getMessage(), e);
        // }

       
    }

    @Override
    public void onRSSIPackageReceived(int i) {
        LOG.i("MederTransceiver", "onRSSIPackageReceived: " + i);

        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_rssireceived', {'rssi': " + i + "});");



        // early exit if callbackContext is not yet set
        if(this._callbackContext == null) return;
        // send PluginResult
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);

        // // the message object
        // JSONObject info = new JSONObject();
        // try {
        //     info.put("type", "rssi");
        //     info.put("rssi", i);
        // } catch (JSONException e) {
        //     LOG.i("RTXMessageReceivedListenerListener", e.getMessage(), e);
        // }

    }

    @Override
    public void onFirmwareReceived(String firmware) {

    }

    @Override
    public void onRTXConfigurationReceived(RTXConfiguration rtxConfiguration) {

    }

}
