package com.fluxguide.cordova.plugin;

import com.medercommtech.smartguide.rtxserviceapi.RTXServiceEventListener;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

public class RTXServiceEventListenerListener implements RTXServiceEventListener {

    private CallbackContext _callbackContext;
    private MederTransceiver _API;

    public RTXServiceEventListenerListener(CallbackContext callbackContext, MederTransceiver API) {
        this._callbackContext = callbackContext;
        this._API = API;
    }

    @Override
    public void onServiceConnected() {

        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_serviceconnected');");

        // early exit if callbackContext is not yet set
        if(this._callbackContext == null) return;
        // send PluginResult
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);

        // // the message object
        // JSONObject info = new JSONObject();
        // try {
        //     info.put("type", "serviceconnected");
        // } catch (JSONException e) {
        //     LOG.e("RTXMessageReceivedListenerListener", e.getMessage(), e);
        // }

    }

    @Override
    public void onServiceReady() {

        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_serviceready');");

        // early exit if callbackContext is not yet set
        if(this._callbackContext == null) return;
        // send PluginResult
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);


        // // the message object
        // JSONObject info = new JSONObject();
        // try {
        //     info.put("type", "serviceready");
        // } catch (JSONException e) {
        //     LOG.e("RTXMessageReceivedListenerListener", e.getMessage(), e);
        // }
    }

    @Override
    public void onServiceDisconnected() {

        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_servicedisconnected');");

        // early exit if callbackContext is not yet set
        if(this._callbackContext == null) return;
        // send PluginResult
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);


        // // the message object
        // JSONObject info = new JSONObject();
        // try {
        //     info.put("type", "servicedisconnected");
        // } catch (JSONException e) {
        //     LOG.e("RTXMessageReceivedListenerListener", e.getMessage(), e);
        // }
    }

    @Override
    public void onServiceError(String error) {

        this._API.sendJavascript("cordova.fireDocumentEvent('medertransceiver_serviceerror');");

        // early exit if callbackContext is not yet set
        if(this._callbackContext == null) return;
        // send PluginResult
        PluginResult result = new PluginResult(PluginResult.Status.OK);
        result.setKeepCallback(true);
        this._callbackContext.sendPluginResult(result);


        // // the message object
        // JSONObject info = new JSONObject();
        // try {
        //     info.put("type", "serviceerror");
        // } catch (JSONException e) {
        //     LOG.e("RTXMessageReceivedListenerListener", e.getMessage(), e);
        // }
    }

}
