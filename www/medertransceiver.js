cordova.define('cordova-plugin-meder-transceiver.MederTransceiver', function(require, exports, module) {
    var exec = require('cordova/exec')
    var cordova = require('cordova')
    var channel = require('cordova/channel')
    var utils = require('cordova/utils')

    function MederTransceiver() {}

    MederTransceiver.prototype.VERSION = 1

    // Define Modes
    MederTransceiver.prototype.mode = {}
    MederTransceiver.prototype.mode['RECEIVE'] = 'RECEIVE'
    MederTransceiver.prototype.mode['SEND'] = 'SEND'

    // Define Modes
    MederTransceiver.prototype.rtxmode = {}
    MederTransceiver.prototype.rtxmode['NONE'] = 'NONE'
    MederTransceiver.prototype.rtxmode['COMMAND_RECEIVE'] = 'COMMAND_RECEIVE'
    MederTransceiver.prototype.rtxmode['COMMAND_SEND'] = 'COMMAND_SEND'
    MederTransceiver.prototype.rtxmode['LISTEN_HEADPHONES'] = 'LISTEN_HEADPHONES'
    MederTransceiver.prototype.rtxmode['LISTEN_SPEAKER_BACK'] = 'LISTEN_SPEAKER_BACK'
    MederTransceiver.prototype.rtxmode['LISTEN_SPEAKER_FRONT'] = 'LISTEN_SPEAKER_FRONT'
    MederTransceiver.prototype.rtxmode['SPEAK_EXTERNAL_MIC'] = 'SPEAK_EXTERNAL_MIC'
    MederTransceiver.prototype.rtxmode['SPEAK_INTERNAL_MIC'] = 'SPEAK_INTERNAL_MIC'

    // Define Paths
    MederTransceiver.prototype.rtxpath = {}
    MederTransceiver.prototype.rtxpath['ANALOG'] = 'ANALOG'
    MederTransceiver.prototype.rtxpath['DIGITAL'] = 'DIGITAL'


    MederTransceiver.prototype.trigger_rssireceived = function() {
        console.log("trigger_rssireceived");
       //cordova.fireDocumentEvent('medertransceiver_rssireceived')
    }

    /**
     * Setup function
     * initializes the RTX Service
     * @param {function} success_cb the success callback after setup is completed
     * @param {function} error_cb error callback when setup failed
     * @fires datareceived
     * @fires rssireceived
     */
    MederTransceiver.prototype.setup = function(success_cb, error_cb) {
        cordova.exec(
            function(success) {
                // fire default success callback
                if (typeof success_cb === 'function') success_cb(success)         
                return
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'SETUP', // action
            [] // arguments
        )
    }

    /**
     * Change to a certain channel
     * @param {number} channel the channel (from 1 to 30)
     * @param {function} success_cb
     * @param {function} error_cb
     */
    MederTransceiver.prototype.change_channel = function(channel, success_cb, error_cb) {
        if (channel == undefined) throw new Error('channel may not be undefined')
        if (typeof channel !== 'number') throw new Error('channel must be a number')
        if (channel < 0) throw new Error('channel may not be lower than 0')
        if (channel > 30) throw new Error('channel may not be higher than 30')

        cordova.exec(
            function(success) {
                if (typeof success_cb === 'function') success_cb(success)
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'CHANGE_CHANNEL', // action
            [channel] // arguments
        )
    }

    /**
     * Set the volume for audio receiving modes (LISTEN_HEADPHONES, LISTEN_SPEAKER_FRONT, LISTEN_SPEAKER_BACK)
     * @param {number} volume must be between 0-1 (0 = mute, 1 = max volume)
     * @param {function} success_cb
     * @param {function} error_cb
     */
    MederTransceiver.prototype.set_volume = function(volume, success_cb, error_cb) {
        let _volume = volume * 10 // Meder Library needs values from 0-10

        cordova.exec(
            function(success) {
                if (typeof success_cb === 'function') success_cb(success)
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'SET_VOLUME', // action
            [_volume] // arguments
        )
    }

    /**
     * Set the mode for the Meder Transceiver.
     * @param {MederTransceiver.rtxmode} rtxmode the mode to set the module to. One of type: MederTransceiver.rtxmode.*
     * @param {function} success_cb
     * @param {function} error_cb
     */
    MederTransceiver.prototype.set_mode = function(rtxmode, success_cb, error_cb) {
        cordova.exec(
            function(success) {
                if (typeof success_cb === 'function') success_cb(success)
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'SET_RTX_MODE', // action
            [rtxmode] // arguments
        )
    }

    /**
     * Send a data message
     * The Transceiver must be in one of these modes: MederTransceiver.rtxmode.SPEAK_EXTERNAL_MIC, MederTransceiver.rtxmode.SPEAK_INTERNAL_MIC, MederTransceiver.rtxmode.COMMAND_SEND
     * @param {string} data the data (payload) to send.
     *                      Max length:
     *                          - 16 byte (characters) when in MederTransceiver.rtxmode.SPEAK_EXTERNAL_MIC or MederTransceiver.rtxmode.SPEAK_INTERNAL_MIC
     *                          - 80 byte (characters) when in MederTransceiver.rtxmode.COMMAND_SEND
     * @param {function} success_cb
     * @param {function} error_cb
     */
    MederTransceiver.prototype.send_data = function(data, success_cb, error_cb) {
        cordova.exec(
            function(success) {
                if (typeof success_cb === 'function') success_cb(success)
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'SEND_COMMAND', // action
            [data] // arguments
        )
    }

    /**
     * Set the RTX Path (ANALOG or DIGITAL)
     * Analog is faster (no latency) but lower audio quality, Digital has a significant delay but better audio quality
     * @param {string} rtxpath RTXPath, one of: MederTransceiver.rtxpath.ANALOG or MederTransceiver.rtxpath.DIGITAL
     * @param {function} success_cb
     * @param {function} error_cb
     */
    MederTransceiver.prototype.set_rtx_path = function(rtxpath, success_cb, error_cb) {
        cordova.exec(
            function(success) {
                if (typeof success_cb === 'function') success_cb(success)
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'SET_RTX_PATH', // action
            [rtxpath] // arguments
        )
    }

    /**
     * Set the microphone noise gate on or off. Default value is off.
     * @param {boolean} enabled
     * @param {function} success_cb
     * @param {function} error_cb
     */
    MederTransceiver.prototype.set_mic_noise_gate = function(enabled, success_cb, error_cb) {
        cordova.exec(
            function(success) {
                if (typeof success_cb === 'function') success_cb(success)
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'SET_MIC_NOISE_GATE', // action
            [enabled] // arguments
        )
    }

    /**
     * Set the microphone sensitivity when in MederTransceiver.mode.SPEAK_INTERNAL_MIC or MederTransceiver.mode.SPEAK_EXTERNAL_MIC mode.
     * @param {number} sensitivity Can be in the range 1 - 7. 1 for low sensitivity and 7 for high sensitivity. The default value is 3.
     * @param {function} success_cb
     * @param {function} error_cb
     */
    MederTransceiver.prototype.set_mic_sensitivity = function(sensitivity, success_cb, error_cb) {
        cordova.exec(
            function(success) {
                if (typeof success_cb === 'function') success_cb(success)
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'SET_MIC_SENSITIVITY', // action
            [sensitivity] // arguments
        )
    }

    /**
     * This method unbinds from the RTX Service provided by the SGManager App. If no other activity is bound to the Service it gets destroyed and RTX is disabled.
     * @param {function} success_cb
     * @param {function} error_cb
     */
    MederTransceiver.prototype.unbind_rtx_service = function(success_cb, error_cb) {
        cordova.exec(
            function(success) {
                if (typeof success_cb === 'function') success_cb(success)
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'UNBIND_RTX_SERVICE', // action
            [] // arguments
        )
    }

    MederTransceiver.prototype.enable_ir_receive_mode = function(success_cb, error_cb) {
        cordova.exec(
            function(success) {
                // fire default success callback
                if (typeof success_cb === 'function') success_cb(success)
                return
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'ENABLE_IR_RECEIVE_MODE', // action
            [] // arguments
        )
    }

    MederTransceiver.prototype.disable_ir_receive_mode = function(success_cb, error_cb) {
        cordova.exec(
            function(success) {
                // fire default success callback
                if (typeof success_cb === 'function') success_cb(success)
                return
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'DISABLE_IR_RECEIVE_MODE', // action
            [] // arguments
        )
    }

    MederTransceiver.prototype.send_ir_command = function(command, success_cb, error_cb) {
        cordova.exec(
            function(success) {
                // fire default success callback
                if (typeof success_cb === 'function') success_cb(success)
                return
            },
            function(err) {
                if (typeof error_cb === 'function') error_cb(err)
            },
            'MederTransceiver', // class
            'SEND_IR_COMMAND', // action
            [command] // arguments
        )
    }

    module.exports = new MederTransceiver()
})
